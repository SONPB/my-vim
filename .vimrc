set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'aradunovic/perun.vim'
Plugin 'crusoexia/vim-javascript-lib'
call vundle#end()  
filetype plugin indent on   

set runtimepath+=~/.vim_runtime



autocmd vimenter * NERDTree


source ~/.vim_runtime/vimrcs/basic.vim
source ~/.vim_runtime/vimrcs/filetypes.vim
source ~/.vim_runtime/vimrcs/plugins_config.vim
source ~/.vim_runtime/vimrcs/extended.vim
set mouse=a
set number
set t_Co=256
let g:Powerline_symbols = 'fancy'

let NERDTreeMapActivateNode='v'

try
syntax enable
" colorscheme monokai
colorscheme monokai


map <C-n> :NERDTreeToggle<CR>
source ~/.vim_runtime/my_configs.vim
catch
endtry
